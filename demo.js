// bài 1: Tiền Lương
function money() {
    const inputValue = document.getElementById("day").value
    const result = inputValue * 100000
    document.getElementById("resultMoney").innerHTML = result
}

// bài 2: Trung bình cộng
function number() {
    const inputNumber1 = document.getElementById("number_1").valueAsNumber
    const inputNumber2 = document.getElementById("number_2").valueAsNumber
    const inputNumber3 = document.getElementById("number_3").valueAsNumber
    const inputNumber4 = document.getElementById("number_4").valueAsNumber
    const inputNumber5 = document.getElementById("number_5").valueAsNumber
    const inputNumber = document.getElementById("number").valueAsNumber
    console.log(inputNumber1, inputNumber2, inputNumber3, inputNumber4, inputNumber5, inputNumber)
    const result = (inputNumber1 + inputNumber2 + inputNumber3 + inputNumber4 + inputNumber5) / inputNumber
    document.getElementById("resultNumber").innerHTML = result
}


// bài 3: USD => VND
function change() {
    const inputChange = document.getElementById("moneyusd").value
    const result = inputChange * 23500
    document.getElementById("resultChange").innerHTML = result
}

// bài 4: Tính chu vi, diện tích hình chữ nhật

// Tính chu vi 
function chuvi() {
    const inputChuvi1 = document.getElementById("numberchuvi1").valueAsNumber
    const inputChuvi2 = document.getElementById("numberchuvi2").valueAsNumber
    const inputChuvi = document.getElementById("numberchuvi").valueAsNumber
    const result = (inputChuvi1 + inputChuvi2) * inputChuvi
    document.getElementById("resultChuvi").innerHTML = result
}

// Tính diện tích
function dientich() {
    const inputDientich1 = document.getElementById("numberdientich1").valueAsNumber
    const inputDientich2 = document.getElementById("numberdientich2").valueAsNumber
    const result = inputDientich1 * inputDientich2
    document.getElementById("resultDientich").innerHTML = result

}


//bài 5: Tình tổng 2 ký số
function tinhtong() {
    const inputTinhtong = document.getElementById("numberso1").valueAsNumber
    const so_hang_dv = inputTinhtong % 10
    const so_hang_chuc = inputTinhtong / 10
    const result = so_hang_dv + Math.floor(so_hang_chuc)
    document.getElementById("resultKyso").innerHTML = result
}